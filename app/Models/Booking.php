<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $fillable=['hotel_id','room_id','user_id','date','booking_status','payment','booking_code','todate','invoice_id','amount'];
}
