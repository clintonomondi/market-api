<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdsSubcategory extends Model
{
    use HasFactory;

    protected $fillable=['ads_id','subcategory_id'];
}
