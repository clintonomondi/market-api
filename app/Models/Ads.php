<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ads extends Model
{
    use HasFactory;

    protected $fillable=['category_id','country','region','location','title','description','promoted_at',
        'expires_at','code','status','period','created_by','updated_by','user_id','price','phone1','phone2','website'
    ,'model','automotive','makes','year','condition','mileage','gear','uuid','promoted','brand'];
}
