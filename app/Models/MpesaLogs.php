<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MpesaLogs extends Model
{
    use HasFactory;
    protected $fillable=['mpesa_ref','system_ref','MerchantRequestID','CheckoutRequestID','amount','account','ResponseCode','ResponseDescription','status','order_id','phone',
        'ResultCode','ResultDesc'];
}
