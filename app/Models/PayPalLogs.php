<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PayPalLogs extends Model
{
    use HasFactory;

    protected  $fillable=['ads_id','paypal_trans_id','payer_email','given_name','surname','payer_id','trans_status','update_time','country_code','paypal_amount','trans_id'];

}
