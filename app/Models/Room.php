<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Room extends Model
{
    use HasFactory;

    protected  $fillable=['hotel_id','name','rooms','beds','price','description','created_by','updated_by','status','uuid'];
}
