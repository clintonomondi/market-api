<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Hotel;
use App\Models\Invoice;
use App\Models\Room;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BookingController extends Controller
{
    public  function reserve(Request $request){
        $room=Room::find($request->room_id);
        $bookings=DB::select( DB::raw("SELECT COUNT(*)vcount FROM bookings WHERE (('$request->date' BETWEEN date AND todate)
OR  ('$request->todate' BETWEEN date AND todate) OR (date BETWEEN '$request->date' AND '$request->todate')
 OR (todate BETWEEN '$request->date' AND '$request->todate')) AND (booking_status='ACTIVE' OR booking_status='OCCUPIED') AND (room_id='$room->id') "));

//        return $bookings;
        if($bookings[0]->vcount>0){
            return ['status'=>false,'message'=>'The room is already reserved within period specified'];
        }
        $datetime1 = new DateTime($request->date);
        $datetime2 = new DateTime($request->todate);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        if($days<=0){
            $request['amount']=$room->price;
        }else{
            $request['amount']=$days * $room->price;
        }



        $request['booking_code']='ML'.mt_rand(100000,999999);
        $request['user_id']=Auth::user()->id;
        $booking=Booking::create($request->all());
        return ['status'=>true,'message'=>'Reservation done successfully','booking_id'=>$booking->id];
    }

    public  function getBookingsHotel(){
        $hotel_id=Auth::user()->hotel_id;
        $bookings=DB::select( DB::raw("SELECT *,
(SELECT name from rooms B WHERE B.id=A.room_id)name,
(SELECT name from users B WHERE B.id=A.user_id)client_name,
(SELECT phone from users B WHERE B.id=A.user_id)client_phone,
(SELECT price from rooms B WHERE B.id=A.room_id)price
 FROM bookings A WHERE hotel_id='$hotel_id' AND (booking_status='ACTIVE' OR booking_status='OCCUPIED') ORDER BY id desc"));
        return ['bookings'=>$bookings];
    }

    public  function getRecentBookingsHotel(){
        $hotel_id=Auth::user()->hotel_id;
        $bookings=DB::select( DB::raw("SELECT *,
(SELECT name from rooms B WHERE B.id=A.room_id)name,
(SELECT name from users B WHERE B.id=A.user_id)client_name,
(SELECT phone from users B WHERE B.id=A.user_id)client_phone
 FROM bookings A WHERE hotel_id='$hotel_id' AND (booking_status='ACTIVE' OR booking_status='OCCUPIED') ORDER BY id desc LIMIT 5"));

        $today=DB::select( DB::raw("SELECT *  FROM bookings WHERE date(created_at)=CURRENT_DATE"));
        $cancelled=Booking::where('booking_status','!=','ACTIVE')->where('booking_status','!=','USED')->Where('booking_status','!=','OCCUPIED')->count();
        $hotel=Hotel::find($hotel_id);
        $invoice=Invoice::where('hotel_id',$hotel_id)->where('status','PENDING')->sum('amount');
        return ['bookings'=>$bookings,'today'=>$today,'cancelled'=>$cancelled,'hotel'=>$hotel,'invoice'=>$invoice];
    }

    public  function changeBookingStatus(Request $request,$id){
        $booking=Booking::find($id);
        $request['booking_status']=$request->status;
        $booking->update($request->all());
        return ['status'=>true,'message'=>'Booking updated successfully'];
    }

    public  function getBookingByID($id){
        $b=Booking::find($id);
        $booking=DB::select( DB::raw("SELECT *,
       (SELECT email from users B WHERE B.id=A.user_id)email,
       (SELECT name from users B WHERE B.id=A.user_id)client_name,
(SELECT phone from users B WHERE B.id=A.user_id)phone,
(SELECT hotel_name from hotels B WHERE B.id=A.hotel_id)hotel_name,
(SELECT name from rooms B WHERE B.id=A.room_id)room_name,
(SELECT price from rooms B WHERE B.id=A.room_id)price
       FROM bookings A WHERE id='$id'"));

        $datetime1 = new DateTime($b->date);
        $datetime2 = new DateTime($b->todate);
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');

        return ['booking'=>$booking,'days'=>$days];
    }

    public  function myBookings(){
        $id=Auth::user()->id;
        $booking=DB::select( DB::raw("SELECT *,
       (SELECT email from users B WHERE B.id=A.user_id)email,
(SELECT phone from users B WHERE B.id=A.user_id)phone,
(SELECT hotel_name from hotels B WHERE B.id=A.hotel_id)hotel_name,
(SELECT name from rooms B WHERE B.id=A.hotel_id)room_name,
(SELECT price from rooms B WHERE B.id=A.hotel_id)price
       FROM bookings A WHERE user_id='$id' ORDER BY id DESC"));

        return ['bookings'=>$booking];
    }
}
