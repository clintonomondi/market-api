<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\HotelPics;
use App\Models\Room;
use App\Models\RoomPics;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class RoomController extends Controller
{
    public  function saveRooms(Request $request){
        $request['hotel_id']=Auth::user()->hotel_id;
        $request['created_by']=Auth::user()->id;
        $request['uuid']=Str::random(25).Auth::user()->id;
        $room=Room::create($request->all());

        return ['status'=>true,'message'=>'Room update successfully, saving images now......','room_id'=>$room->id];
    }

    public  function pushPhotosForRoom(Request $request){
        try {
            if(!empty($request['files'])) {
                foreach ($request['files'] as $base64File) {
                    $data = substr($base64File, strpos($base64File, ',') + 1);
                    $file = base64_decode($data);
                    $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'jpg';
                    Storage::disk('public')->put('Rooms/' . $fileNameToStore, $file);
                    $request['url'] = $fileNameToStore;
                    $doc = RoomPics::create($request->all());
                }
            }
            return ['status'=>true,'message'=>'Process completed successfully'];
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    public  function pushPhotosForHotel(Request $request){
        try {
            if(!empty($request['files'])) {
                foreach ($request['files'] as $base64File) {
                    $data = substr($base64File, strpos($base64File, ',') + 1);
                    $file = base64_decode($data);
                    $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'jpg';
                    Storage::disk('public')->put('Hotels/' . $fileNameToStore, $file);
                    $request['url'] = $fileNameToStore;
                    $doc = HotelPics::create($request->all());
                }
            }
            return ['status'=>true,'message'=>'Process completed successfully'];
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    public  function getMyRooms(){
        $rooms=Room::where('hotel_id',Auth::user()->hotel_id)->get();
        return ['rooms'=>$rooms];
    }

    public  function getMyRoomByUuid($id){
        $room=Room::where('uuid',$id)->first();
        $pics=RoomPics::where('room_id',$room->id)->get();
        return ['room'=>$room,'pics'=>$pics];
    }

    public  function removeImageForRoom($id){
        $image=RoomPics::find($id);
        $image->delete();
        Storage::delete('/public/Rooms/'.$image->url);
        return ['status'=>true,'message'=>'File removed successfully'];
    }
    public  function removeImageForHotel($id){
        $image=HotelPics::find($id);
        $image->delete();
        Storage::delete('/public/Hotels/'.$image->url);
        return ['status'=>true,'message'=>'File removed successfully'];
    }

    public  function updateRooms(Request $request){
     $room=Room::find($request->id);
     $room->update($request->all());
        return ['status'=>true,'message'=>'Room update successfully, saving images now......','room_id'=>$room->id];
    }

    public  function getHotels(){
        $ads=DB::select( DB::raw(" SELECT *,
(SELECT url FROM hotel_pics B WHERE B.hotel_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `hotels` A"));
        return ['ads'=>$ads];
    }

    public  function getHotelsTrending(){
        $ads=DB::select( DB::raw(" SELECT *,
(SELECT url FROM hotel_pics B WHERE B.hotel_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `hotels` A ORDER BY RAND() LIMIT 4 "));
        return ['ads'=>$ads];
    }

    public  function hotelsByRegion(Request  $request){
        $ads=DB::select( DB::raw(" SELECT *,
(SELECT url FROM hotel_pics B WHERE B.hotel_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `hotels` A WHERE region='$request->region'"));
        return ['ads'=>$ads];
    }
    public  function getHotelBySimpleSearch(Request  $request){
        $ads=DB::select( DB::raw(" SELECT *,
(SELECT url FROM hotel_pics B WHERE B.hotel_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `hotels` A WHERE region LIKE '%$request->keyword%' OR location LIKE '%$request->keyword%' OR hotel_name LIKE  '%$request->keyword%'"));
        return ['ads'=>$ads];
    }
    public  function getHotelPhotos(){
        $id=Auth::user()->id;
        $pics=HotelPics::where('hotel_id',Auth::user()->hotel_id)->get();
        $data=DB::select( DB::raw("SELECT *,
(SELECT hotel_name from hotels B WHERE B.id=A.hotel_id)hotel_name,
(SELECT region from hotels B WHERE B.id=A.hotel_id)region,
(SELECT country from hotels B WHERE B.id=A.hotel_id)country,
(SELECT location from hotels B WHERE B.id=A.hotel_id)location,
(SELECT contact from hotels B WHERE B.id=A.hotel_id)contact
 FROM users A WHERE id='$id'"));
        return ['pics'=>$pics,'hotel_id'=>Auth::user()->hotel_id,'data'=>$data];
    }

    public  function getHotelRooms($id){
        $hotel=Hotel::where('code',$id)->first();
        $ads=DB::select( DB::raw(" SELECT *,
(SELECT url FROM room_pics B WHERE B.room_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `rooms` A WHERE hotel_id='$hotel->id'"));
        return ['ads'=>$ads,'hotel'=>$hotel];
    }

    public  function getRoomByuuid($id){

       $room=Room::where('uuid',$id)->first();
        $pics=RoomPics::where('room_id',$room->id)->get();
        $hotel=Hotel::where('id',$room->hotel_id)->first();
        return ['room'=>$room,'pics'=>$pics,'hotel'=>$hotel];
    }
}
