<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use App\Models\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class InvoiceController extends Controller
{
    public  function generateInvoice(Request  $request,$id){
        $request['hotel_id']=$id;
        $request['created_by']=Auth::user()->id;
        $request['amount']=$request->sum;
       $hotel=Hotel::find($id);
        $request['invoice_no']= strtoupper(substr($hotel->hotel_name, 0, 3).mt_rand(10000000,99999999).date("Y"));
        $invoice=Invoice::create($request->all());

        $bookings=DB::select( DB::raw("UPDATE  bookings SET invoice_id='$invoice->id',payment='INVOICED' WHERE hotel_id='$id' AND payment='PENDING' AND (booking_status='USED' OR booking_status='OCCUPIED') ORDER BY id desc"));
        return ['status'=>true,'message'=>'Invoice generated  successfully'];
    }

    public  function getInvoices(){
        $invoices=DB::select( DB::raw("SELECT *,
        (SELECT hotel_name from hotels B WHERE B.id=A.hotel_id)hotel_name,
        (SELECT COUNT(*) from bookings B WHERE B.invoice_id=A.id)records,
        (SELECT name from users B WHERE B.id=A.created_by)user_name
         FROM invoices A ORDER BY status DESC"));

        return ['invoices'=>$invoices];
    }

    public  function getInvoicesHotel(){
        $hotel_id=Auth::user()->hotel_id;
        $invoices=DB::select( DB::raw("SELECT *,
        (SELECT hotel_name from hotels B WHERE B.id=A.hotel_id)hotel_name,
        (SELECT COUNT(*) from bookings B WHERE B.invoice_id=A.id)records,
        (SELECT name from users B WHERE B.id=A.created_by)user_name
         FROM invoices A WHERE hotel_id='$hotel_id' ORDER BY status DESC"));

        return ['invoices'=>$invoices];
    }

    public  function getInvoiceData($id){
        $invoice=DB::select( DB::raw("SELECT *,
       (SELECT hotel_name from hotels B WHERE B.id= A.hotel_id)hotel_name,
       (SELECT percentage from hotels B WHERE B.id= A.hotel_id)percentage,
       (SELECT region from hotels B WHERE B.id= A.hotel_id)region,
       (SELECT location from hotels B WHERE B.id= A.hotel_id)location,
       (SELECT contact from hotels B WHERE B.id= A.hotel_id)contact
       FROM invoices A WHERE id='$id'"));
        $records=DB::select( DB::raw("SELECT *,
       (SELECT name from rooms B WHERE B.id=A.room_id)room_name,
       (SELECT price from rooms B WHERE B.id=A.room_id)price
       FROM bookings A WHERE invoice_id='$id'"));

        return ['invoice'=>$invoice,'records'=>$records];
    }
}
