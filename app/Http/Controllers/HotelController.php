<?php

namespace App\Http\Controllers;

use App\Models\Hotel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HotelController extends Controller
{
    public  function getAllHotels(){
        $hotels=DB::select( DB::raw("SELECT *,
       (SELECT COUNT(*) from rooms B WHERE B.hotel_id=A.id )rooms,
       (SELECT COUNT(*) FROM bookings B WHERE payment='PENDING' AND (booking_status='OCCUPIED' OR booking_status='USED') AND B.hotel_id=A.id )bookings
       FROM hotels A ORDER BY id desc"));
        return ['hotels'=>$hotels];
    }

    public  function getHotelActiveBookings($id){
        $bookings=DB::select( DB::raw("SELECT *,
(SELECT name from rooms B WHERE B.id=A.room_id)name,
(SELECT price from rooms B WHERE B.id=A.room_id)price
 FROM bookings A WHERE hotel_id='$id' AND payment='PENDING' AND (booking_status='USED' OR booking_status='OCCUPIED') ORDER BY id desc"));

        $hotel=Hotel::find($id);
        return ['bookings'=>$bookings,'hotel'=>$hotel];
    }

    public  function adminUpdateHotel(Request  $request,$id){
        $validated = $request->validate([
            'percentage' => 'required|numeric',
        ]);
        $hotel=Hotel::find($id);
        $hotel->update($request->all());
        return ['status'=>true,'message'=>'Hotel updated successfully'];
    }
}
