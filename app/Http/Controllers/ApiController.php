<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\Booking;
use App\Models\Hotel;
use App\Models\Invoice;
use App\Models\MpesaLogs;
use App\Models\PayPalLogs;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller
{
    public  function indexData(){
        $year=date("Y");
        $amount=MpesaLogs::where('status','PAID')->sum('amount');
        $paid_ads=Ads::where('status','PAID')->count();
        $pending_ads=Ads::where('status','PENDING')->count();
        $total_ads=Ads::count();
        $users=User::where('user_type','user')->count();
        $card=PayPalLogs::sum('paypal_amount');
        $hotels=Hotel::count();
        $bookings=Booking::where('payment','PENDING')->count();

        $data=DB::select( DB::raw("SELECT
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='1'  AND YEAR(updated_at)='$year')jan,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='2'  AND YEAR(updated_at)='$year')feb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='3'  AND YEAR(updated_at)='$year')mar,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='4'  AND YEAR(updated_at)='$year')apr,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='5' AND YEAR(updated_at)='$year')may,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='6'  AND YEAR(updated_at)='$year')jun,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='7'  AND YEAR(updated_at)='$year')jul,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='8'  AND YEAR(updated_at)='$year')aug,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='9'  AND YEAR(updated_at)='$year')sep,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='10'  AND YEAR(updated_at)='$year')octb,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='11'  AND YEAR(updated_at)='$year')nov,
(SELECT IF(SUM(amount) IS NULL,1,SUM(amount)) FROM mpesa_logs WHERE STATUS='PAID' AND   MONTH(updated_at)='12'  AND YEAR(updated_at)='$year')dece
 FROM DUAL"));
        $invoice_pending=Invoice::where('status','PENDING')->sum('amount');
        $invoice_paid=Invoice::where('status','PAID')->sum('amount');

        return ['amount'=>$amount,'pending_ads'=>$pending_ads,'paid_ads'=>$paid_ads,'card'=>$card,'total_ads'=>$total_ads,'users'=>$users,'hotels'=>$hotels,'bookings'=>$bookings,
            'info'=>$data,'invoice_pending'=>$invoice_pending,'invoice_paid'=>$invoice_paid];
    }

    public  function getAccessToken(){
        $value = Cache::remember('paypal_access_token', '28000', function () {
            $user_name=env('PAYPAL_USERNAME');
            $password=env('PAYPAL_PASSWORD');
            $url=env('PAYPAL_ACCESS_TOKEN_URL');
            $client = new Client();
            $response = $client->request('POST', $url,
                [
                    'auth' => [$user_name, $password],
                    'headers' => [
                        'Content-Type'=>'application/x-www-form-urlencoded',
                    ],
                    'form_params' => [
                        'grant_type'=>'client_credentials',
                    ],
                ]
            );
            $data=json_decode($response->getBody()->getContents());
            return $data->access_token;
        });
        return  'Bearer    '.$value;
    }

    public function getProductsToEmailSend(){
        $ads=DB::select( DB::raw("SELECT *,
        (SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
        FROM ads A WHERE   id IN (SELECT ads_id FROM ads_pics) AND status='PAID'  ORDER BY RAND() DESC LIMIT 4"));

        return $ads;
    }
}
