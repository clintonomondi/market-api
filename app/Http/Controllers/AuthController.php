<?php

namespace App\Http\Controllers;

use App\Models\Deactivation;
use App\Models\Hotel;
use App\Models\OTP;
use App\Models\User;
use Carbon\Carbon;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Knox\AFT\AFT;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class AuthController extends Controller
{
    public function signup(Request $request)
    {
        $request['pic']='/images/index.png';
        $request->validate([
            'email' => 'required|string|email',
            'phone' => 'required',
        ]);
        $credentials = request(['email', 'password']);
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email is already in use'];
        }
        if($request->flag=='hotel'){
            $request['code']=Str::random(28);
            $hotel=Hotel::create($request->all());
            $request['hotel_id']=$hotel->id;
            $request['user_type']='hotel';
        }
        $request['password']=bcrypt($request->password);
        $user=User::create($request->all());
        if(!Auth::attempt($credentials)) {
            return ['status' => false, 'message' => 'We are not able to log you in, please use login page'];
        }
        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        $hotel=Hotel::where('id',$profile->hotel_id)->first();

        $ads=(new ApiController)->getProductsToEmailSend();
        $prod1=$ads[0];
        $prod2=$ads[1];

        $EMAIL_SERVICE=env('EMAIL_SERVICE');
        $data=[
            'subject'=>'Welcome to Marketlinkus',
            // 'password'=>$code,
            'email'=>$request->email,
            'product1'=>$prod1->title,
            'product2'=>$prod2->title,
            'price1'=>$prod1->price,
            'price2'=>$prod2->price,
            'link1'=>'https://marketlinkus.com/feeds/detailed/'.$prod1->uuid,
            'link2'=>'https://marketlinkus.com/feeds/detailed/'.$prod2->uuid,
            'image1'=>'https://api.marketlinkus.com/storage/Ads/'.$prod1->url,
            'image2'=>'https://api.marketlinkus.com/storage/Ads/'.$prod2->url,
            ];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($EMAIL_SERVICE.'/welcome',$data);

        
        return ['status'=>true,'hotel'=>$hotel,'user'=>$profile,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];


    }

    public  function attemptLogin(Request $request){
        $email=User::where('email',$request->email)->count();
        if($email>0){
            $u=User::where('email',$request->email)->first();
            $user =User::find($u->id);
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            $profile=User::find($u->id);
            $profile->update($request->all());
            return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken, 'token_type' => 'Bearer', 'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()];
        }else{
            $user=User::create($request->all());
            $tokenResult =$user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            if ($request->remember_me)
                $token->expires_at = Carbon::now()->addWeeks(1);
            $token->save();
            $profile=User::find($user->id);


            $ads=(new ApiController)->getProductsToEmailSend();
        $prod1=$ads[0];
        $prod2=$ads[1];

        $EMAIL_SERVICE=env('EMAIL_SERVICE');
        $data=[
            'subject'=>'Welcome to Marketlinkus',
            // 'password'=>$code,
            'email'=>$request->email,
            'product1'=>$prod1->title,
            'product2'=>$prod2->title,
            'price1'=>$prod1->price,
            'price2'=>$prod2->price,
            'link1'=>'https://marketlinkus.com/feeds/detailed/'.$prod1->uuid,
            'link2'=>'https://marketlinkus.com/feeds/detailed/'.$prod2->uuid,
            'image1'=>'https://api.marketlinkus.com/storage/Ads/'.$prod1->url,
            'image2'=>'https://api.marketlinkus.com/storage/Ads/'.$prod2->url,
            ];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($EMAIL_SERVICE.'welcome',$data);

            return ['status'=>true,'user'=>$profile,'access_token' => $tokenResult->accessToken,
                'token_type' => 'Bearer',
                'expires_at' => Carbon::parse(
                    $tokenResult->token->expires_at
                )->toDateTimeString()

            ];

        }
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);
        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return ['status'=>false,'message' => 'Invalid email or password'];

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        $profile=User::find(Auth::user()->id);
        $hotel=Hotel::where('id',$profile->hotel_id)->first();

        return ['status'=>true,'user'=>$profile,'hotel'=>$hotel,'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()

        ];
    }

    public  function getCode(Request $request){
        $check=User::where('email',$request->email)->count();
        $user=User::where('email',$request->email)->first();
        if($check<=0){
            return ['status'=>false,'message'=>'The email address does not exist'];
        }
        $request['phone']=$user->phone;
        $request['email']=$user->email;
        $randomid = mt_rand(1000,9999);
        $request['code']=$randomid;
        $data=OTP::create($request->all());

        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='Your four digit  verification code is '.$randomid;
        AFT::sendMessage($phone, $message);
        return ['status'=>true,'user'=>$user,'message'=>'A  verification code has been sent to your phone,enter the code'];
    }

    public function confirm(Request $request){
        $datas = DB::select( DB::raw("SELECT * FROM `o_t_p_s` WHERE phone='$request->phone'  AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
        $datas2 = DB::select( DB::raw("SELECT * FROM `o_t_p_s` WHERE  email='$request->email' AND code='$request->code' AND created_at > NOW() - INTERVAL 4 HOUR") );
        if($datas==null && $datas2==null){
            return ['status'=>false,'message'=>'Incorrect code entered'];
        }
        return ['status'=>true,'message'=>'Success'];
    }

    public  function setPassword(Request $request){
        $user=User::find($request->id);
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='Yes';
        $user->update($request->all());

        $datas = DB::select( DB::raw("DELETE FROM `o_t_p_s` WHERE phone='$request->phone'  OR email='$request->email'") );
        return ['status'=>true,'message'=>'Password changed successfully, please login'];
    }

    public  function users(){
        $users=User::where('user_type','admin')->get();
        return ['users'=>$users];
    }
    public  function clients(){
        $users=User::orderBy('id','desc')->where('user_type','user')->get();
        return ['users'=>$users];
    }

    public  function createUsers(Request $request){
        $request['pass']=mt_rand(1000,9999);
        $request['password']=bcrypt($request->pass);
        $request['user_type']='admin';
        $user=User::create($request->all());

        if(strlen($user->phone)==10){
            $phone=$user->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($user->phone,4));
        }
        $message='You have been added at marketlink as super admin,Use the code bellow as initial password.Please remember to change this password '.$request->pass;
        AFT::sendMessage($phone, $message);

        return ['status'=>true,'message'=>'User created successfully'];

    }

    public  function updateUsers(Request $request){
        $user=User::find($request->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }
    public  function updateUser(Request $request,$id){
        $user=User::find($id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }
    public  function getProfile(){
        $user=User::find(Auth::id());
        return ['user'=>$user];
    }

    public  function updateProfile(Request $request){
        if(empty($request->phone)){
            return ['status'=>false,'message'=>'Update phone number'];
        }
        $user=User::find(Auth::id());
        $user->update($request->all());
        return ['status'=>true,'message'=>'Profile updated successfully'];
    }

    public  function updatePassword(Request $request){
        if($request->password!=$request->repass){
            return ['status'=>false,'message'=>'Password do not match'];
        }

        $currentpass = auth()->user()->password;
        if (!Hash::check($request['currentpass'], $currentpass)) {
            return ['status'=>false,'message'=>'The current password is invalid'];
        }
        $request['password']=bcrypt($request->password);
        $request['password_changed']='YES';
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        return ['status'=>true,'message'=>'Password updated successfully'];
    }

    public  function getUsers(){
        $users=User::where('hotel_id',Auth::user()->hotel_id)->get();
        return ['users'=>$users];
    }

    public  function addUser(Request $request){
        $email=User::where('email',$request->email)->count();
        if($email>0){
            return ['status'=>false,'message'=>'Email address is already in use'];
        }
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';
        $request['user_name']='NO';
        $request['user_type']='hotel';
        $request['hotel_id']=Auth::user()->hotel_id;
        $request['uuid']=Str::random(25);
        if(strlen($request->phone)==10){
            $phone=$request->phone;
        }else{
            $phone=str_replace(' ','','0'.substr($request->phone,4));
        }
        $message='You been on boarded on marketlink us hotel as admin. Your password is  '.$code;
        AFT::sendMessage($phone, $message);
        $user=User::create($request->all());

        return ['status'=>true,'message'=>'User added successfully'];
    }

    public  function changeUserStatus(Request $request,$id){
        if($id==Auth::user()->id){
            return ['status'=>false,'message'=>'You are not authorised to deactivate your account'];
        }
        $user=User::find($id);
        if($request->status=='Inactive'){
            if(empty($request->reason)){
                return ['status'=>false,'message'=>'Please provide reason for deactivating this user'];
            }
            $request['user_id']=$id;
            Deactivation::create($request->all());
        }
        $user->update($request->all());
        return ['status'=>true,'message'=>'User updated successfully'];
    }

    public  function updateHotelProfile(Request $request){
        $user=User::find(Auth::user()->id);
        $user->update($request->all());
        $hote=Hotel::find($user->hotel_id);
        $hote->update($request->all());
        return ['status'=>true,'message'=>'Profile updated successfully'];
    }

    public  function forget(Request $request){
        $data=User::where('email',$request->email)->count();
        if($data<=0){
            return ['status'=>false,'message'=>'The email you entered does not exist'];
        }
        $user=User::where('email',$request->email)->first();
        $code=rand(100000,999999);
        $request['password']=bcrypt($code);
        $request['password_changed']='NO';
        $ads=(new ApiController)->getProductsToEmailSend();
        $prod1=$ads[0];
        $prod2=$ads[1];


        $EMAIL_SERVICE=env('EMAIL_SERVICE');
        $data=[
            'subject'=>'Password Reset',
            'password'=>$code,
            'email'=>$request->email,
            'product1'=>$prod1->title,
            'product2'=>$prod2->title,
            'price1'=>$prod1->price,
            'price2'=>$prod2->price,
            'link1'=>'https://marketlinkus.com/feeds/detailed/'.$prod1->uuid,
            'link2'=>'https://marketlinkus.com/feeds/detailed/'.$prod2->uuid,
            'image1'=>'https://api.marketlinkus.com/storage/Ads/'.$prod1->url,
            'image2'=>'https://api.marketlinkus.com/storage/Ads/'.$prod2->url,
            ];
        $response = Http::withHeaders(['Content-Type'=>'application/json'])->post($EMAIL_SERVICE.'/password',$data);
        Log::info($response);
        $d=User::where('email',$request->email)->update(['password'=>$request->password,'password_changed'=>$request->password_changed]);
        return ['status'=>true,'message'=>'A new password has been sent to your email.','response'=>json_decode($response)];

    }
    

}
