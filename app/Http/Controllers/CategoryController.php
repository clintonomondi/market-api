<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    public  function post(Request $request){
        $request['created_by']=Auth::user()->id;
        $request['updated_by']=Auth::user()->id;

        $request->validate([
            'pic' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1999',
        ]);
        //get file name with extension
        $fileNameWithExt=$request->file('pic')->getClientOriginalName();
        $filename=pathinfo($fileNameWithExt,PATHINFO_FILENAME);
        $extesion=$request->file('pic')->getClientOriginalExtension();
        //filename to store
        $fileNameToStore=$filename.'_'.time().'.'.$extesion;
        //uploadimage
        $path=$request->file('pic')->storeAs('/public/backgrounds',$fileNameToStore);
        $request['picture']=$fileNameToStore;
        $data=Category::create($request->all());
        return ['status'=>true,'message'=>'Category created successfully'];
    }
    public  function get(){
        $cats=DB::select( DB::raw("SELECT *,
         (SELECT count(*) FROM subcategories B WHERE B.category_id=A.id)subcat,
          (SELECT count(*) FROM ads B WHERE B.category_id=A.id)ads
          FROM categories A  ORDER BY ABS(onemonth) DESC ") );
        return ['cats'=>$cats];
    }

    public  function update(Request $request){
        $data=Category::find($request->id);
     if (($request->has('pic')) && !empty($request->pic)) {
     //get file name with extension
     $fileNameWithExt = $request->file('pic')->getClientOriginalName();
     $filename = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
     $extesion = $request->file('pic')->getClientOriginalExtension();
     //filename to store
     $fileNameToStore = $filename . '_' . time() . '.' . $extesion;
     //uploadimage
     $path = $request->file('pic')->storeAs('/public/backgrounds', $fileNameToStore);
     $request['picture'] = $fileNameToStore;
     Storage::delete('/public/backgrounds/'.$data->picture);
    }
     $data->update($request->all());
        return ['status'=>true,'message'=>'Category updated successfully'];
    }


public  function postothercat(Request $request){
        $data=Subcategory::create($request->all());
         return ['status'=>true,'message'=>'SubCategory updated successfully'];
}

public  function getothercat($id){
    $category=Category::find($id);
    $allAds=Ads::where('category_id',$id)->where('status','PAID')->count();
    $cats=DB::select( DB::raw("SELECT *,
(SELECT COUNT(*)vcount from ads_subcategories B WHERE B.subcategory_id=A.id AND ads_id IN (SELECT id from ads WHERE status='PAID'))ads
 FROM subcategories A WHERE category_id='$id' ") );
    return ['cats'=>$cats,'category'=>$category,'allAds'=>$allAds];
}

public  function updateothercat(Request $request){
        $data=Subcategory::find($request->id);
        $data->update($request->all());
    return ['status'=>true,'message'=>'SubCategory updated successfully'];
}

}
