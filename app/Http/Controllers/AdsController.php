<?php

namespace App\Http\Controllers;

use App\Jobs\AdImageProcessing;
use App\Models\Ads;
use App\Models\AdsPics;
use App\Models\AdsSubcategory;
use App\Models\Category;
use App\Models\PayPalLogs;
use App\Models\User;
use App\Models\Viewers;
use DateTime;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Knox\AFT\AFT;

class AdsController extends Controller
{
    public  function submitadd(Request $request){
        try {
        if(empty($request->category_id)){
         return ['status'=>false,'message'=>'Select category'];
        }
        if(empty($request->country)){
            return ['status'=>false,'message'=>'Select country'];
        }
        if(empty($request->region)){
            return ['status'=>false,'message'=>'Select region'];
        }
        if(empty($request->location)){
            return ['status'=>false,'message'=>'Provide location'];
        }
        if(empty($request->title)){
            return ['status'=>false,'message'=>'Provide title'];
        }
        if(empty($request->description)){
            return ['status'=>false,'message'=>'Please give description'];
        }
        if(empty($request->phone1)){
            return ['status'=>false,'message'=>'Provide the first contact no.'];
        }
        $request['code']='ML'.mt_rand(10000,99999).Auth::user()->id;
        $request['created_by']=Auth::user()->id;
        $request['user_id']=Auth::user()->id;
        $request['uuid']=Str::random(15).Auth::user()->id;

        $request['status']='PAID';

        $ads=Ads::create($request->all());
        if(!empty($request->subcategory_id)) {
            foreach ($request->subcategory_id as $subcat_i) {
                $request['subcategory_id']=$subcat_i;
                $request['ads_id']=$ads->id;
                AdsSubcategory::create($request->all());
            }
        }
            Log::info('ads saved successfully');
        return ['status'=>true,'message'=>'Ads created successfully, wait as we are saving images...','ads_id'=>$ads->id];
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    public  function updateadd(Request $request){
        $ads=Ads::find($request->id);
        $request['updated_by']=Auth::user()->id;
        $ads->update($request->all());

        if(!empty($request->subcategory_id)) {
            foreach ($request->subcategory_id as $subcat_i) {
                $request['subcategory_id']=$subcat_i;
                $request['ads_id']=$ads->id;
                AdsSubcategory::create($request->all());
            }
        }
        return ['status'=>true,'message'=>'Ads submitted successfully, wait as we are trying to save images...','ads_id'=>$ads->id];
    }

    public  function pushPhotos(Request $request){
            try {
                if(!empty($request['files'])) {
                    foreach ($request['files'] as $base64File) {
                        $data = substr($base64File, strpos($base64File, ',') + 1);
                        $file = base64_decode($data);
                        $fileNameToStore = mt_rand(10000, 99999) . '_' . time() . '.' . 'jpg';
                        Storage::disk('public')->put('Ads/' . $fileNameToStore, $file);
                        $request['url'] = $fileNameToStore;
                        $doc = AdsPics::create($request->all());
                    }
                }
            return ['status'=>true,'message'=>'Process completed successfully'];
       } catch (\Exception $e) {
            Log::info($e->getMessage());
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    public  function getAdInfo($id){
        $ad=Ads::find($id);
        $cat=Category::find($ad->category_id);
        return ['ads'=>$ad,'cat'=>$cat];
    }

    public  function myAdds(){
        $id=Auth::user()->id;
        $ads=DB::select( DB::raw("SELECT *,
(SELECT name FROM categories B WHERE B.id=A.category_id)category
 FROM ads A  WHERE user_id='$id' order by id desc "));
        return ['ads'=>$ads];
    }

    public  function getAdsByCat(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
 FROM ads A  WHERE status='PAID' AND category_id='$request->category_id' order by promoted desc "));
        return ['ads'=>$ads];
    }
    public  function getAdsByNewest(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
 FROM ads A  WHERE status='PAID' AND category_id='$request->category_id' order by id desc "));
        return ['ads'=>$ads];
    }

    public  function getAdsByLowestprice(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
 FROM ads A  WHERE status='PAID' AND category_id='$request->category_id' order by ABS(price) asc "));
        return ['ads'=>$ads];
    }
    public  function getAdsByHigestprice(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
 FROM ads A  WHERE status='PAID' AND category_id='$request->category_id' order by ABS(price) desc "));
        return ['ads'=>$ads];
    }

    public  function getAdsBySubcat(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `ads` A WHERE id IN (SELECT ads_id FROM `ads_subcategories` WHERE subcategory_id='$request->subcategory_id') AND status='PAID' order by promoted desc"));
        return ['ads'=>$ads];
    }
    public  function adsByRegion(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `ads` A WHERE id IN (SELECT ads_id FROM `ads_subcategories` WHERE category_id='$request->category_id') AND status='PAID' AND region='$request->region' order by promoted desc"));
        return ['ads'=>$ads];
    }
    public function getAdsBySimpleSearch(Request $request){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
FROM `ads` A WHERE category_id='$request->category_id' AND  status='PAID' AND   (location LIKE '%$request->keyword%' OR model LIKE '%$request->keyword%' OR brand LIKE '%$request->keyword%'
OR makes LIKE '%$request->keyword%' OR title LIKE '%$request->keyword%')"));
        return ['ads'=>$ads];
    }
public  function getAdsByrandomSearch(Request $request){
    $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id LIMIT 1)url
FROM `ads` A WHERE   status='PAID' AND  (location LIKE '%$request->search%' OR model LIKE '%$request->search%' OR brand LIKE '%$request->search%'
OR makes LIKE '%$request->search%' OR title LIKE '%$request->search%' OR region LIKE '%$request->search%' OR category_id IN (SELECT id FROM categories WHERE name LIKE '%$request->search%')
OR id IN (SELECT ads_id     FROM ads_subcategories WHERE subcategory_id IN (SELECT id FROM subcategories WHERE name LIKE '%$request->search%')))"));
    return ['ads'=>$ads];
}
    public  function getTrendingAds(){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url,
(SELECT COUNT(*)vcount FROM ads_pics B WHERE B.ads_id=A.id)vcount
 FROM ads A WHERE   id IN (SELECT ads_id FROM ads_pics)
AND status='PAID' AND ABS(price)>=1000000 ORDER BY id DESC LIMIT 12"));

        $ads2=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url,
(SELECT COUNT(*)vcount FROM ads_pics B WHERE B.ads_id=A.id)vcount
 FROM ads A WHERE  status='PAID'  ORDER BY RAND() DESC LIMIT 12"));

        return ['ads'=>$ads,'ads2'=>$ads2];
    }

    public  function getAdsById(Request $request){
        $ads=Ads::find($request->ads_id);
        $owner=User::where('id',$ads->user_id)->first();
        $pics=AdsPics::where('ads_id',$request->ads_id)->get();
        $subcats=AdsSubcategory::where('ads_id',$request->ads_id)->get();
        $views=Viewers::where('ads_id',$request->ads_id)->count();
        return ['ads'=>$ads,'pics'=>$pics,'owner'=>$owner,'subcats'=>$subcats,'views'=>$views];
    }
    public  function getAdsByuuid(Request $request){
        $ads=Ads::where('uuid',$request->uuid)->first();
        $owner=User::where('id',$ads->user_id)->first();
        $pics=AdsPics::where('ads_id',$ads->id)->get();
        $subcats=AdsSubcategory::where('ads_id',$ads->id)->get();
        $views=Viewers::where('ads_id',$ads->id)->count();
        $similar=DB::select( DB::raw("SELECT *,
(SELECT url FROM ads_pics B WHERE B.ads_id=A.id ORDER BY RAND() LIMIT 1)url
 FROM ads A  WHERE status='PAID' AND category_id='$ads->category_id' AND id!='$ads->id' ORDER BY RAND() LIMIT 20"));

        $datetime1 = new DateTime($ads->created_at);
        $datetime2 = new DateTime(Carbon::now());
        $interval = $datetime1->diff($datetime2);
        $days = $interval->format('%a');
        $cat=Category::find($ads->category_id);

        return ['ads'=>$ads,'pics'=>$pics,'owner'=>$owner,'subcats'=>$subcats,'views'=>$views,'similar'=>$similar,'days'=>$days,'cat'=>$cat];
    }

    public  function removeImage($id){
        $image=AdsPics::find($id);
        $image->delete();
        Storage::delete('/public/Ads/'.$image->url);
        return ['status'=>true,'message'=>'File removed successfully'];
    }



    public  function payPalcheckout()
    {
        $paypal_access_token= (new ApiController)->getAccessToken();
        $paypal_client_token_url=env('PAYPAL_CLIENT_TOKEN_URL');
        $client_id=env('CLIENT_ID');
        return ['paypal_client_token_url'=>$paypal_client_token_url,'paypal_access_token'=>$paypal_access_token,'client_id'=>$client_id];
    }

    public  function orderStatus(Request $request,$id){
        $ads=Ads::find($id);
        $request['ads_id']=$id;
        $request['user_id']=Auth::user()->id;
        $request['trans_id']=substr(md5(microtime()), 0, 10);
        $request['promoted']='YES';
        $request['reason']='Ads Payment';
        $request['amount_paid']=$request->paypal_amount;
        $request['status'] = 'PAID';
        $current =new Carbon($request->promoted_at);
        $trialExpires = $current->addDays($request->days);
        $request['expires_at']=$trialExpires->format('Y-m-d H:i:s');
        $ads->update($request->all());
        $paypal_logs=PayPalLogs::create($request->all());

        $admins=User::where('user_type','admin')->get();
        foreach ($admins as $admin){
            if(strlen($admin->phone)==10){
                $phone=$admin->phone;
            }else{
                $phone=str_replace(' ','','0'.substr($admin->phone,4));
            }
            $message='A client with phone no '.Auth::user()->phone.' has PAID an ad '.'. @marketlinkus';
            AFT::sendMessage($phone, $message);
        }
        return ['status'=>true,'message'=>'Payment successfully made.Redirecting...'];
    }

    public  function saveViewer(Request $request){
        $count=Viewers::where('ip_address',$request->ip_address)->where('ads_id',$request->ads_id)->count();
        if($count<=0){
            $d=Viewers::create($request->all());
        }
        return ['status'=>true];
    }

    public  function allAds(){
        $ads=DB::select( DB::raw("SELECT *,
(SELECT name FROM categories B WHERE B.id=A.category_id)category,
(SELECT name FROM users B WHERE B.id=A.user_id)username,
(SELECT email FROM users B WHERE B.id=A.user_id)useremail
 FROM ads A  order by id desc"));
        return ['ads'=>$ads];
    }

    public  function updateAdsStatus(Request $request){
        $ads=Ads::find($request->id);
        $ads->update($request->all());
        return ['status'=>true,'message'=>'Ads updated successfully'];
    }
}
