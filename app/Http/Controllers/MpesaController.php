<?php

namespace App\Http\Controllers;

use App\Models\Ads;
use App\Models\MpesaLogs;
use App\Models\TimeManager;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use MPESA;
use Knox\AFT\AFT;

class MpesaController extends Controller
{
    public  function initiateSTK(Request $request,$id){
        try {
            $order=Ads::find($id);
            if(strlen($request->phone)==10){
                $phone='254'.substr($request->phone,1);
            }else{
                $phone=str_replace(' ','','254'.substr($request['Body']['stkCallback']->phone,4));
            }
            $mpesa = MPESA::stkPush((int)$phone,(int)$request->amount,$order->code);

            $request['system_ref']='ML'.mt_rand(10000,99999).Auth::user()->id;
            $request['MerchantRequestID']=$mpesa->MerchantReque['Body']['stkCallback']
        } catch (\Exception $e) {
            return ['status'=>false,'ResponseMessage'=>$e->getMessage()];
        }
    }
    public  function checkStkPayment(Request $request){
        $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
        if($info->status=='PAID'){
            return ['status'=>true,'mpesa_ref'=>$info->mpesa_ref,'callback'=>true];
        }else if($info->status=='CANCELLED'){
           return ['status'=>true,'message'=>'User cancelled payment.Pleaese wait.....','callback'=>false];
        }
        else{
            return ['status'=>false];
        }

    }


    public  function callBack(Request $request){
        try {
            $response = $request->all();
    Log::info($response['Body']['stkCallback']);
            $request['ResultCode'] = $response['Body']['stkCallback']['ResultCode'];
            $request['ResultDesc'] = $response['Body']['stkCallback']['ResultDesc'];
            $request['MerchantRequestID'] = $response['Body']['stkCallback']['MerchantRequestID'];
            $request['CheckoutRequestID'] = $response['Body']['['Body']['stkCallback']stkCallback']['CheckoutRequestID'];
            if ($request->ResultCode == '0') {
                $collection = collect($response['Body']['stkCallback']['CallbackMetadata']['Item']);
                $transaction = $collection->where('Name', 'MpesaReceiptNumber')->first();
                $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=MpesaLogs::find($info->id);
                $order=Ads::find($logs->order_id);
                $request['status']='PAID';
                $request['promoted']='YES';
                $request['mpesa_ref']=$transaction['Value'];
                $request['trans_id']=$transaction['Value'];
                $request['payment_ref']=$transaction['Value'];
                $request['system_id']=$logs->system_ref;
                $request['payment_method']='M-PESA EXPRESS';
                $logs->update($request->all());
                $order->update($request->all());
                $user=User::find($order->user_id);

                if(strlen($user->phone)==10){
                    $phone=$user->phone;
                }else{
                    $phone=str_replace(' ','','0'.substr($user->phone,4));
                }
                $message='Your payment for Ads '.$order->code.' has been received.M-Pesa REF: '.$request->mpesa_ref.' .System REF: '.$request->system_id.'Thank you.';
                AFT::sendMessage($phone, $message);

                $admins=User::where('user_type','admin')->get();
                foreach ($admins as $admin){
                    if(strlen($admin->phone)==10){
                        $phone=$admin->phone;
                    }else{
                        $phone=str_replace(' ','','0'.substr($admin->phone,4));
                    }
                    $message='A client with phone no '.$user->phone.' has PAID an ads '.'. @marketlinkus';
                    AFT::sendMessage($phone, $message);
                }

            }else{
                $info=MpesaLogs::where('MerchantRequestID',$request->MerchantRequestID)->where('CheckoutRequestID',$request->CheckoutRequestID)->first();
                $logs=MpesaLogs::find($info->id);
                $request['status']='CANCELLED';
                $logs->update($request->all());
            }
        } catch (\Exception $e) {
            Log::info($e->getMessage());
        }
    }

}
