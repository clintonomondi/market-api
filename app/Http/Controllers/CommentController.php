<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public  function postComment(Request  $request){
        $comment=Comment::create($request->all());
        return ['status'=>true,'message'=>'Comment submited successfully'];
    }
}
