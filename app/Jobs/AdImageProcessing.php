<?php

namespace App\Jobs;

use App\Models\AdsPics;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class AdImageProcessing implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $request = null;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        foreach ($this->request['files'] as $base64File) {
            $data = substr($base64File, strpos($base64File, ',') + 1);
            $file = base64_decode($data);
            $fileNameToStore = mt_rand(10000,99999) . '_' . time() . '.' .'jpg';
            Storage::disk('public')->put('Ads/'.$fileNameToStore, $file);
//            $request['url'] = $fileNameToStore;
            $doc = AdsPics::create(['url'=>$fileNameToStore,'ads_id'=>$this->request['ads_id']]);
        }

    }
}
