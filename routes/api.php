<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;



Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'App\Http\Controllers\AuthController@login');
    Route::post('attemptLogin', 'App\Http\Controllers\AuthController@attemptLogin');
    Route::post('signup', 'App\Http\Controllers\AuthController@signup');
    Route::get('category', 'App\Http\Controllers\CategoryController@get');
    Route::post('getAdsByCat', 'App\Http\Controllers\AdsController@getAdsByCat');
    Route::post('getAdsBySubcat', 'App\Http\Controllers\AdsController@getAdsBySubcat');
    Route::get('getothercat/{id}', 'App\Http\Controllers\CategoryController@getothercat');
    Route::post('getAdsById', 'App\Http\Controllers\AdsController@getAdsById');
    Route::post('getAdsByuuid', 'App\Http\Controllers\AdsController@getAdsByuuid');
    Route::post('adsByRegion', 'App\Http\Controllers\AdsController@adsByRegion');
    Route::post('getAdsByNewest', 'App\Http\Controllers\AdsController@getAdsByNewest');
    Route::post('getAdsByLowestprice', 'App\Http\Controllers\AdsController@getAdsByLowestprice');
    Route::post('getAdsByHigestprice', 'App\Http\Controllers\AdsController@getAdsByHigestprice');
    Route::post('getAdsBySimpleSearch', 'App\Http\Controllers\AdsController@getAdsBySimpleSearch');
    Route::post('getAdsByrandomSearch', 'App\Http\Controllers\AdsController@getAdsByrandomSearch');
    Route::post('saveViewer', 'App\Http\Controllers\AdsController@saveViewer');
    Route::get('getTrendingAds', 'App\Http\Controllers\AdsController@getTrendingAds');
//sms
    Route::post('getCode', 'App\Http\Controllers\AuthController@getCode');
    Route::post('confirm', 'App\Http\Controllers\AuthController@confirm');
    Route::post('setPassword', 'App\Http\Controllers\AuthController@setPassword');
    Route::post('forget', 'App\Http\Controllers\AuthController@forget');

    Route::post('callBack', 'App\Http\Controllers\MpesaController@callBack');

    Route::get('getHotels', 'App\Http\Controllers\RoomController@getHotels');
    Route::get('getHotelsTrending', 'App\Http\Controllers\RoomController@getHotelsTrending');
    Route::post('hotelsByRegion', 'App\Http\Controllers\RoomController@hotelsByRegion');
    Route::post('getHotelBySimpleSearch', 'App\Http\Controllers\RoomController@getHotelBySimpleSearch');
    Route::get('getHotelRooms/{id}', 'App\Http\Controllers\RoomController@getHotelRooms');
    Route::get('getRoomByuuid/{id}', 'App\Http\Controllers\RoomController@getRoomByuuid');


    Route::post('postComment', 'App\Http\Controllers\CommentController@postComment');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'App\Http\Controllers\AuthController@logout');
        //category
        Route::post('category', 'App\Http\Controllers\CategoryController@post');
        Route::post('category/update', 'App\Http\Controllers\CategoryController@update');
        Route::post('postothercat', 'App\Http\Controllers\CategoryController@postothercat');
        Route::post('updateothercat', 'App\Http\Controllers\CategoryController@updateothercat');


        //Ads
        Route::post('submitadd', 'App\Http\Controllers\AdsController@submitadd');
        Route::post('updateadd', 'App\Http\Controllers\AdsController@updateadd');
        Route::post('pushPhotos', 'App\Http\Controllers\AdsController@pushPhotos');
        Route::get('getAdInfo/{id}', 'App\Http\Controllers\AdsController@getAdInfo');
        Route::get('myAdds', 'App\Http\Controllers\AdsController@myAdds');
        Route::get('removeImage/{id}', 'App\Http\Controllers\AdsController@removeImage');
        Route::get('payPalcheckout', 'App\Http\Controllers\AdsController@payPalcheckout');
        Route::post('orderStatus/{id}', 'App\Http\Controllers\AdsController@orderStatus');
        Route::get('allAds', 'App\Http\Controllers\AdsController@allAds');
        Route::get('allAds', 'App\Http\Controllers\AdsController@allAds');
        Route::post('updateAdsStatus', 'App\Http\Controllers\AdsController@updateAdsStatus');



        //Mpesa
        Route::post('initiateSTK/{id}', 'App\Http\Controllers\MpesaController@initiateSTK');
        Route::post('checkStkPayment', 'App\Http\Controllers\MpesaController@checkStkPayment');

        //Index
        Route::get('admin/indexData', 'App\Http\Controllers\ApiController@indexData');
        Route::get('users', 'App\Http\Controllers\AuthController@users');
        Route::get('clients', 'App\Http\Controllers\AuthController@clients');
        Route::post('createUsers', 'App\Http\Controllers\AuthController@createUsers');
        Route::post('updateUsers', 'App\Http\Controllers\AuthController@updateUsers');
        Route::post('updateUser/{id}', 'App\Http\Controllers\AuthController@updateUser');
        Route::post('updatePassword', 'App\Http\Controllers\AuthController@updatePassword');

        //profile
        Route::get('getProfile', 'App\Http\Controllers\AuthController@getProfile');
        Route::post('updateProfile', 'App\Http\Controllers\AuthController@updateProfile');

        //Rooms
        Route::post('saveRooms', 'App\Http\Controllers\RoomController@saveRooms');
        Route::post('pushPhotosForRoom', 'App\Http\Controllers\RoomController@pushPhotosForRoom');
        Route::post('pushPhotosForHotel', 'App\Http\Controllers\RoomController@pushPhotosForHotel');
        Route::get('getMyRooms', 'App\Http\Controllers\RoomController@getMyRooms');
        Route::get('getMyRoomByUuid/{id}', 'App\Http\Controllers\RoomController@getMyRoomByUuid');
        Route::get('removeImageForRoom/{id}', 'App\Http\Controllers\RoomController@removeImageForRoom');
        Route::get('removeImageForHotel/{id}', 'App\Http\Controllers\RoomController@removeImageForHotel');
        Route::post('updateRooms', 'App\Http\Controllers\RoomController@updateRooms');
        Route::get('getHotelPhotos', 'App\Http\Controllers\RoomController@getHotelPhotos');
        Route::get('getUsers', 'App\Http\Controllers\AuthController@getUsers');
        Route::post('updateHotelProfile', 'App\Http\Controllers\AuthController@updateHotelProfile');

        //
        Route::post('reserve', 'App\Http\Controllers\BookingController@reserve');
        Route::post('addUser', 'App\Http\Controllers\AuthController@addUser');
        Route::post('changeUserStatus/{id}', 'App\Http\Controllers\AuthController@changeUserStatus');
        Route::post('changeUserStatus/{id}', 'App\Http\Controllers\AuthController@changeUserStatus');
        Route::get('getBookingsHotel', 'App\Http\Controllers\BookingController@getBookingsHotel');
        Route::get('getRecentBookingsHotel', 'App\Http\Controllers\BookingController@getRecentBookingsHotel');
        Route::post('changeBookingStatus/{id}', 'App\Http\Controllers\BookingController@changeBookingStatus');
        Route::get('getBookingByID/{id}', 'App\Http\Controllers\BookingController@getBookingByID');
        Route::get('myBookings', 'App\Http\Controllers\BookingController@myBookings');

        //Hotels
        Route::get('getAllHotels', 'App\Http\Controllers\HotelController@getAllHotels');
        Route::get('getHotelActiveBookings/{id}', 'App\Http\Controllers\HotelController@getHotelActiveBookings');
        Route::post('adminUpdateHotel/{id}', 'App\Http\Controllers\HotelController@adminUpdateHotel');

        //Invoices
        Route::post('generateInvoice/{id}', 'App\Http\Controllers\InvoiceController@generateInvoice');
        Route::get('getInvoices', 'App\Http\Controllers\InvoiceController@getInvoices');
        Route::get('getInvoicesHotel', 'App\Http\Controllers\InvoiceController@getInvoicesHotel');
        Route::get('getInvoiceData/{id}', 'App\Http\Controllers\InvoiceController@getInvoiceData');

    });
});

