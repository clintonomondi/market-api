<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class AdsSubcatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,30) as $index) {
            DB::table('ads_subcategories')->insert([
                'ads_id' => $faker->numberBetween(1,500),
                'subcategory_id' => $faker->numberBetween(1,30),
            ]);
        }
    }
}
