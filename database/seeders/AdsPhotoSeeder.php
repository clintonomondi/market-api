<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class AdsPhotoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,1000) as $index) {
            DB::table('ads_pics')->insert([
                'ads_id' => $faker->numberBetween(1,500),
                'url' => $faker->randomElement($array = array ('bg2_1619861074.jpg','bg4_1619861075.jpg','s1_1619888333.jpg')),
            ]);
        }
    }
}
