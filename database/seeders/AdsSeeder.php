<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
class AdsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        foreach (range(1,500) as $index) {
            DB::table('ads')->insert([
                'user_id' => $faker->numberBetween(1,50),
                'category_id' => $faker->numberBetween(1,20),
                'country' => 'Kenya',
                'region' => $faker->randomElement($array = array ('Nairobi City','Homa Bay','Kisumu')),
                'location' => $faker->randomElement($array = array ('Greenspan','Donholm','Kayoye','CBD','Thika','Kilimani','Kangemi')),
                'title' => $faker->name(),
                'description' => $faker->paragraph(1),
                'promoted_at' => $faker->dateTimeThisYear($max = 'now', $timezone = null) ,
                'expires_at' => $faker->dateTimeThisYear($max = 'now', $timezone = null) ,
                'code' => $faker->randomDigit(),
                'status' => 'PAID',
                'phone1' => '+254712083128',
            ]);
        }
    }
}
