<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayPalLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_pal_logs', function (Blueprint $table) {
            $table->id();
            $table->string('ads_id')->nullable();
            $table->string('paypal_trans_id')->nullable();
            $table->string('payer_email')->nullable();
            $table->string('given_name')->nullable();
            $table->string('surname')->nullable();
            $table->string('payer_id')->nullable();
            $table->string('trans_status')->nullable();
            $table->string('update_time')->nullable();
            $table->string('country_code')->nullable();
            $table->string('paypal_amount')->nullable();
            $table->string('trans_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_pal_logs');
    }
}
